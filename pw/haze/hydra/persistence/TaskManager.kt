package pw.haze.hydra.persistence

import pw.haze.hydra.OS
import pw.haze.hydra.curOS
import java.io.BufferedReader
import java.io.InputStreamReader

/**
 * |> Author: haze
 * |> Since: 4/23/16
 */
class TaskManager : Runnable {
    override fun run() {
        while (true) {
            killTaskManager()
            Thread.sleep(100)
        }
    }


    private fun killTaskManager() {
        fun osx_find(str: List<String>): String{

            fun isNumeric(str: String): Boolean {
                for(c: Char in str){
                    if(!c.isDigit()) return false
                }
                return true
            }

            for(occ in str){
                if(occ.isNullOrEmpty()) continue
                if(isNumeric(occ)) return occ
            }
            return "n/a"
        }

        when (curOS) {
//            OS.WINDOWS -> {
//                val command = Runtime.getRuntime().exec("TASKLIST")
//                val output = BufferedReader(InputStreamReader(command.inputStream))
//                var line = output.readLine();
//                while (line !=null) {
//                    System.out.println(line)
//                    if (line.contains("taskmgr.exe")) {
//                        Runtime.getRuntime().exec("Taskkill /IM taskmgr.exe /F")
//                        break;
//                    }
//                }
//            }

            OS.UNIX -> {
                // -- Print all running processes.
                // -- Pair <Name, PID>
                val process = Runtime.getRuntime().exec("ps -e")
                val input = BufferedReader(InputStreamReader(process.inputStream))
                var line = input.readLine()
                while (line != null) {
                    if (!line.contains("PID") && !line.contains("TTY") && !line.contains("CMD")) {
                        var name: String
                        val pid = osx_find(line.split(" ")).toInt()
                        if (line.contains('/')) {
                            val split = line.split("/")
                            name = split[split.size - 1].split(" ")[0]
                        } else {
                            val split = line.split(" ");
                            name = split[split.size - 1].split(" ")[0]
                        }
                        if(name.contains("Activity")) {
                            Runtime.getRuntime().exec("kill $pid") }
                        }
                    line = input.readLine()
                }
            }
        }
    }

}