package pw.haze.hydra

/**
 * |> Author: haze
 * |> Since: 4/23/16
 */


enum class OS { WINDOWS, UNIX }


val curOS = getOS()

private fun getOS(): OS {
    val os = System.getProperty("os.name")
    return when {
        os.contains("Windows") -> OS.WINDOWS
        else -> OS.UNIX
    }
}

